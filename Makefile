.PHONY: codegen

codegen:
	python -m grpc_tools.protoc -I protos/ --python_out=helloworld/ --grpc_python_out=helloworld/ helloworld.proto
